package org.tworoads.mae.service;

import com.google.common.collect.Sets;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.tworoads.mae.model.*;
import org.tworoads.mae.repository.*;

import java.util.Collection;
import java.util.Set;

/**
 * Created by pc on 2/9/2017.
 */
@Service
@Transactional
public class MaeService {
    @Autowired
    private TaskRepository taskRepository;
    @Autowired
    private LinkTagRepository linkTagrepository;
    @Autowired
    private ExtentTagRepository extentTagrepository;
    @Autowired
    private TagTypeRepository tagTypeRepository;
    @Autowired
    private CharIndexRepository charIndexRepository;
    @Autowired
    private AttributeRepository attributeRepository;
    @Autowired
    private ArgumentRepository argumentRepository;

    public Task createTask(Task task){
        task = taskRepository.save(task);
        Collection<TagType> savedTagTypes = saveTagTypesAndAssignToLinkAndExtentTags(task);
        extentTagrepository.save(task.getExtentTags());
        linkTagrepository.save(task.getLinkTags());
        saveCharIndexes(task.getExtentTags());
        saveAttributes(task, savedTagTypes);
        saveArguments(task, savedTagTypes);
        return task;
    }

    public void saveCharIndexes(Collection<ExtentTag> tags){
        tags.forEach(extentTag -> {
            extentTag.getSpans().forEach(span -> span.setTag(extentTag));
            charIndexRepository.save(extentTag.getSpans());
        });
    }

    public Collection<TagType> saveTagTypesAndAssignToLinkAndExtentTags(Task task){
        Set<TagType> tagTypes = ServiceHelper.setTaskToTagTypes(task);
        tagTypeRepository.save(tagTypes);
        ServiceHelper.setTagTypesToTags(task.getExtentTags(), tagTypes);
        ServiceHelper.setTagTypesToTags(task.getLinkTags(), tagTypes);
        return tagTypes;
    }

    public Collection<Attribute> saveAttributes(Task task, Collection<TagType> tagTypes){
        return attributeRepository.save(ServiceHelper.setTagTypesToAttributes(ServiceHelper.setTagsToAttributes(task), tagTypes));
    }

    private Collection<Argument> saveArguments(Task task, Collection<TagType> tagTypes) {
        return argumentRepository.save(ServiceHelper.setTagTypesToArguments(ServiceHelper.setTagsToArguments(task), tagTypes));
    }
    public boolean delete(Task task){
        taskRepository.delete(task);
        return true;
    }
}
