package org.tworoads.mae.service;

import com.google.common.collect.Sets;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.SetUtils;
import org.tworoads.mae.model.*;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by dguha on 2/11/2017.
 */
public class ServiceHelper {
    public static Set<TagType> setTaskToTagTypes(Task aTask){
        Set<TagType> allTagTypes = Sets.newHashSet();
        aTask.getExtentTags().forEach(tag -> tag.getTagType().setTask(aTask));
        aTask.getLinkTags().forEach(tag -> tag.getTagType().setTask(aTask));
        aTask.getExtentTags().stream().forEach(tag -> tag.getAttributes().stream().forEach(attribute -> setTaskAndAddTagTypeToSet(attribute.getAttributeType(), aTask, allTagTypes)));
        aTask.getLinkTags().stream().forEach(tag -> tag.getArguments().stream().forEach(argument -> setTaskAndAddTagTypeToSet(argument.getArgumentType(), aTask, allTagTypes)));
        aTask.getLinkTags().stream().forEach(tag -> tag.getAttributes().stream().forEach(attribute -> setTaskAndAddTagTypeToSet(attribute.getAttributeType(), aTask, allTagTypes)));
        Set<TagType> tagTypes = Sets.union(aTask.getExtentTags(), aTask.getLinkTags()).stream().map(tag -> tag.getTagType()).collect(Collectors.toSet());
        tagTypes.forEach(tagType -> tagType.setTask(aTask));
        allTagTypes.addAll(tagTypes);
        return allTagTypes;
    }
    private static void setTaskAndAddTagTypeToSet(TagProperty tagPop, Task aTask, Set<TagType> tagTypes){
        TagType tagType = tagPop.getTagType();
        tagType.setTask(aTask);
        tagTypes.add(tagType);
    }

    public static void setTagTypesToTags(Collection<? extends Tag> tags, Set<TagType> tagTypes) {
        tags.stream().forEach(tag -> {
            if(!tagTypes.contains(tag.getTagType()))
                throw new RuntimeException("Tag Types do not match");
            tag.setTagType(tagTypes.stream().filter(tagType -> tagType.equals(tag.getTagType())).findFirst().get());
        });
    }

    public static void setTagTypeToAttributeTypes(Collection<Attribute> attrs, Collection<TagType> tagTypes){
        attrs.stream().forEach(attribute -> {
            if(!tagTypes.contains(attribute.getAttributeType().getTagType()))
                throw new RuntimeException("Tag Types do not match");
            attribute.getAttributeType().setTagType(tagTypes.stream().filter(tagType -> tagType.equals(attribute.getAttributeType().getTagType())).findFirst().get());
        });
    }

    public static void setTagTypeToArgumentTypes(Collection<Argument> attrs, Collection<TagType> tagTypes){
        attrs.stream().forEach(argument -> {
            if(!tagTypes.contains(argument.getArgumentType().getTagType()))
                throw new RuntimeException("Tag Types do not match");
            argument.getArgumentType().setTagType(tagTypes.stream().filter(tagType -> tagType.equals(argument.getArgumentType().getTagType())).findFirst().get());
        });
    }

    public static Collection<Attribute> setTagsToAttributes(Task task) {
        Set<Attribute> attributes = Sets.newHashSet();
        task.getExtentTags().forEach(extentTag -> {
            extentTag.getAttributes().forEach(attribute -> attribute.setExtentTag(extentTag));
            attributes.addAll(extentTag.getAttributes());
        });
        task.getLinkTags().forEach(linkTag -> {
            linkTag.getAttributes().forEach(attribute -> {
                if(attributes.contains(attribute))
                    attributes.stream().filter(attr -> attr.equals(attribute)).findFirst().get().setLinkTag(linkTag);
                else
                    attribute.setLinkTag(linkTag);
            });
            attributes.addAll(linkTag.getAttributes());
        });
        return attributes;
    }
    public static Collection<Argument> setTagsToArguments(Task task) {
        Set<Argument> arguments = Sets.newHashSet();
        task.getLinkTags().forEach(linkTag -> {
            linkTag.getArguments().forEach(argument -> {
                argument.setLinkTag(linkTag);
                argument.setExtentTag(task.getExtentTags().stream().filter(extentTag -> extentTag.getTid().equals(argument.getExtentTid())).findFirst().get());
            });
            arguments.addAll(linkTag.getArguments());
        });
        return arguments;
    }

    public static Collection<Attribute> setTagTypesToAttributes(Collection<Attribute> attributes, Collection<TagType> tagTypes) {
        attributes.stream().forEach(attribute -> attribute.getAttributeType().setTagType(tagTypes.stream().filter(tagType -> tagType.equals(attribute.getAttributeType().getTagType())).findFirst().get()));
        return attributes;
    }

    public static Collection<Argument> setTagTypesToArguments(Collection<Argument> arguments, Collection<TagType> tagTypes) {
        arguments.stream().forEach(argument -> argument.getArgumentType().setTagType(tagTypes.stream().filter(tagType -> tagType.equals(argument.getArgumentType().getTagType())).findFirst().get()));
        return arguments;
    }
}
