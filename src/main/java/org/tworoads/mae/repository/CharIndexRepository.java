package org.tworoads.mae.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.tworoads.mae.model.CharIndex;

/**
 * Created by pc on 2/11/2017.
 */
public interface CharIndexRepository extends JpaRepository<CharIndex, Long> {
}
