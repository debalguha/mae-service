package org.tworoads.mae.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.tworoads.mae.model.Argument;

/**
 * Created by dguha on 2/11/2017.
 */
public interface ArgumentRepository extends JpaRepository<Argument, Long> {
}
