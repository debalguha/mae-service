package org.tworoads.mae.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.tworoads.mae.model.Task;

/**
 * Created by pc on 2/9/2017.
 */
public interface TaskRepository extends JpaRepository<Task, Long> {
}
