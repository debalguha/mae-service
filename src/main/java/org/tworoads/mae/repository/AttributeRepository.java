package org.tworoads.mae.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.tworoads.mae.model.Attribute;

/**
 * Created by dguha on 2/11/2017.
 */
public interface AttributeRepository extends JpaRepository<Attribute, Long> {
}
