package org.tworoads.mae.web;

import com.google.common.collect.Sets;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.tworoads.mae.model.ExtentTag;
import org.tworoads.mae.model.LinkTag;
import org.tworoads.mae.model.Task;
import org.tworoads.mae.service.MaeService;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Collection;

/**
 * Created by dguha on 2/12/2017.
 */
@RestController
public class MaeController {
    @Autowired
    private MaeService service;
    @RequestMapping(value = "/task/create", method = RequestMethod.POST, consumes = {"multipart/form-data"})
    public void createTask(@RequestPart("task") @Valid TaskDTO taskDto,
                           @RequestPart("taskFile") @Valid @NotNull @NotBlank MultipartFile taskFile,
                           @RequestPart("annotationFile") @Valid @NotNull @NotBlank MultipartFile annotationFile) throws Exception{
        Task task = adjustTaskAndTags(taskDto);
        task.setTaskFileContent(taskFile.getBytes());
        task.setAnnotationFileContent(annotationFile.getBytes());
        task = service.createTask(task);
    }

    private Task adjustTaskAndTags(TaskDTO dto){
        Task task = dto.getTask();
        Collection<ExtentTag> extentTags = dto.getExtentTags();
        Collection<LinkTag> linkTags = dto.getLinkTags();
        linkTags.forEach(tag -> tag.setTask(task));
        extentTags.forEach(tag -> tag.setTask(task));
        task.setExtentTags(Sets.newHashSet(extentTags));
        task.setLinkTags(Sets.newHashSet(linkTags));
        return task;
    }
}
