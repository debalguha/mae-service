package org.tworoads.mae.web;

import org.tworoads.mae.model.ExtentTag;
import org.tworoads.mae.model.LinkTag;
import org.tworoads.mae.model.Task;

import java.util.Collection;

/**
 * Created by dguha on 2/12/2017.
 */
public class TaskDTO {
    private Task task;
    private Collection<ExtentTag> extentTags;
    private Collection<LinkTag> linkTags;

    public TaskDTO(Task task, Collection<ExtentTag> extentTags, Collection<LinkTag> linkTags) {
        this.task = task;
        this.extentTags = extentTags;
        this.linkTags = linkTags;
    }
    public TaskDTO(){}

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }

    public Collection<ExtentTag> getExtentTags() {
        return extentTags;
    }

    public void setExtentTags(Collection<ExtentTag> extentTags) {
        this.extentTags = extentTags;
    }

    public Collection<LinkTag> getLinkTags() {
        return linkTags;
    }

    public void setLinkTags(Collection<LinkTag> linkTags) {
        this.linkTags = linkTags;
    }
}
