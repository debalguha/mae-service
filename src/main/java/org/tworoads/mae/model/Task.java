/*
 * MAE - Multi-purpose Annotation Environment
 *
 * Copyright Keigh Rim (krim@brandeis.edu)
 * Department of Computer Science, Brandeis University
 * Original program by Amber Stubbs (astubbs@cs.brandeis.edu)
 *
 * MAE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, @see <a href="http://www.gnu.org/licenses">http://www.gnu.org/licenses</a>.
 *
 * For feedback, reporting bugs, use the project on Github
 * @see <a href="https://github.com/keighrim/mae-annotation">https://github.com/keighrim/mae-annotation</a>.
 */

package org.tworoads.mae.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by krim on 12/27/2015.
 */

@Entity
public class Task implements ModelI {
	
	@Id
	@Column
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

    @Column(nullable = false)
    private String name;

    @Lob
    @Column(columnDefinition = "text")
    private String primaryText;

    @Column(nullable = false, unique = true)
    private String taskFileName;

    @Column(nullable = false, unique = true)
    private String annotationFileName;

    @Lob
    @Column(nullable = false, columnDefinition = "longblob")
    private byte[] taskFileContent;

    @Lob
    @Column(nullable = false, columnDefinition = "longblob")
    private byte[] annotationFileContent;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "task")
    private Set<ExtentTag> extentTags;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "task")
    private Set<LinkTag> linkTags;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "task")
    private Set<TagType> tagTypes;

    public Task() {
    }

    public byte[] getTaskFileContent() {
        return taskFileContent;
    }

    public void setTaskFileContent(byte[] taskFileContent) {
        this.taskFileContent = taskFileContent;
    }

    public byte[] getAnnotationFileContent() {
        return annotationFileContent;
    }

    public void setAnnotationFileContent(byte[] annotationFileContent) {
        this.annotationFileContent = annotationFileContent;
    }

    public Task(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrimaryText() {
        return primaryText;
    }

    public void setPrimaryText(String primaryText) {
        this.primaryText = primaryText;
    }

    public String getTaskFileName() {
        return taskFileName;
    }

    public void setTaskFileName(String taskFileName) {
        this.taskFileName = taskFileName;
    }

    public String getAnnotationFileName() {
        return annotationFileName;
    }

    public void setAnnotationFileName(String annotationFileName) {
        this.annotationFileName = annotationFileName;
    }

    public boolean isTaskLoaded() {
        return getTaskFileName() != null;
    }

    public boolean isAnnotationLoaded() {
        return getAnnotationFileName() != null;
    }

    public boolean isPrimaryTextLoaded() {
        return getPrimaryText() != null;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<ExtentTag> getExtentTags() {
        return extentTags;
    }

    public void setExtentTags(Set<ExtentTag> extentTags) {
        this.extentTags = extentTags;
    }

    public Set<LinkTag> getLinkTags() {
        return linkTags;
    }

    public void setLinkTags(Set<LinkTag> linkTags) {
        this.linkTags = linkTags;
    }

    public Set<TagType> getTagTypes() {
        return tagTypes;
    }

    public void setTagTypes(Set<TagType> tagTypes) {
        this.tagTypes = tagTypes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Task task = (Task) o;

        if (name != null ? !name.equals(task.name) : task.name != null) return false;
        return annotationFileName != null ? annotationFileName.equals(task.annotationFileName) : task.annotationFileName == null;

    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (annotationFileName != null ? annotationFileName.hashCode() : 0);
        return result;
    }
}
