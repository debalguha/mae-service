package org.tworoads.mae.model;

/**
 * Created by pc on 2/11/2017.
 */
public interface ElementType {
    Tag getExtentTag();
    Tag getLinkTag();
}
