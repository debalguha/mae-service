/*
 * MAE - Multi-purpose Annotation Environment
 *
 * Copyright Keigh Rim (krim@brandeis.edu)
 * Department of Computer Science, Brandeis University
 * Original program by Amber Stubbs (astubbs@cs.brandeis.edu)
 *
 * MAE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, @see <a href="http://www.gnu.org/licenses">http://www.gnu.org/licenses</a>.
 *
 * For feedback, reporting bugs, use the project on Github
 * @see <a href="https://github.com/keighrim/mae-annotation">https://github.com/keighrim/mae-annotation</a>.
 */

package org.tworoads.mae.model;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by krim on 11/19/15.
 */

@Entity
public class ExtentTag extends Tag {

    @Column
    private String text;

    @OneToMany(fetch = FetchType.EAGER, mappedBy="tag", cascade = CascadeType.ALL)
    private Set<CharIndex> spans;

    @OneToMany(fetch = FetchType.EAGER, mappedBy="extentTag")
    protected Set<Attribute> attributes;

    @OneToMany(fetch = FetchType.EAGER, mappedBy="extentTag")
    protected Set<Argument> arguments;

    public ExtentTag() {}

    public ExtentTag(String tid, TagType tagType, String filename, Task aTask) {
        super(tid, tagType, filename, aTask);
        this.spans = null;
        this.text = null;
        this.task = aTask;
    }

    public Set<Attribute> getAttributes() {
        return attributes;
    }

    public void setAttributes(Set<Attribute> attributes) {
        this.attributes = attributes;
    }

    public void setSpans(Set<CharIndex> spans) {
        this.spans = spans;
    }

    public Set<CharIndex> getSpans() {
        return spans;
    }


    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Set<Argument> getArguments() {
        return arguments;
    }

    public void setArguments(Set<Argument> arguments) {
        this.arguments = arguments;
    }

    @Override
    public String toString() {
        return String.format("%s (%s)", getId(), getText());
    }


}
