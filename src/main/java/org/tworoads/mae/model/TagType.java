/*
 * MAE - Multi-purpose Annotation Environment
 *
 * Copyright Keigh Rim (krim@brandeis.edu)
 * Department of Computer Science, Brandeis University
 * Original program by Amber Stubbs (astubbs@cs.brandeis.edu)
 *
 * MAE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, @see <a href="http://www.gnu.org/licenses">http://www.gnu.org/licenses</a>.
 *
 * For feedback, reporting bugs, use the project on Github
 * @see <a href="https://github.com/keighrim/mae-annotation">https://github.com/keighrim/mae-annotation</a>.
 */

package org.tworoads.mae.model;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.persistence.*;


/**
 * Created by krim on 11/19/15.
 */

@Entity
@Table(name = "tag_type", indexes = {@Index(name = "tt_uniq_idx", columnList = "name, task_id", unique = true)})
public class TagType implements ModelI {
	
	@Id
	@Column
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	@Column(nullable = false)
    private String name;

	@Column(nullable = false)
    private String prefix;

	@Column(nullable = false)
    private boolean isLink;

	@Column
    private boolean isNonConsuming;
	
    @OneToMany(fetch = FetchType.LAZY, mappedBy="tagType")
    private Set<AttributeType> attributeTypes;

    @OneToMany(fetch = FetchType.LAZY, mappedBy="tagType")
    private Set<ArgumentType> argumentTypes;

    @OneToMany(fetch = FetchType.LAZY, mappedBy="tagType")
    private Set<ExtentTag> extentTags;

    @OneToMany(fetch = FetchType.LAZY, mappedBy="tagType")
    private Set<LinkTag> linkTags;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "task_id", referencedColumnName = "id")
	private Task task;


    public TagType() {}

    public TagType(String name, String prefix, boolean isLink) {
        this.setName(name);
        this.setPrefix(prefix);
        // next two setters should be called in order (setting non-consuming makes the type to be extent)
        this.setNonConsuming(false);
        this.setLink(isLink);

    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public boolean isLink() {
		return isLink;
	}

	public void setLink(boolean isLink) {
		this.isLink = isLink;
	}

	public boolean isNonConsuming() {
		return isNonConsuming;
	}

	public void setNonConsuming(boolean isNonConsuming) {
		this.isNonConsuming = isNonConsuming;
	}

	public Set<AttributeType> getAttributeTypes() {
		return attributeTypes;
	}

	public void setAttributeTypes(Set<AttributeType> attributeTypes) {
		this.attributeTypes = attributeTypes;
	}

	public Set<ArgumentType> getArgumentTypes() {
		return argumentTypes;
	}

	public void setArgumentTypes(Set<ArgumentType> argumentTypes) {
		this.argumentTypes = argumentTypes;
	}

	public Set<ExtentTag> getExtentTags() {
		return extentTags;
	}

	public void setExtentTags(Set<ExtentTag> extentTags) {
		this.extentTags = extentTags;
	}

	public Set<LinkTag> getLinkTags() {
		return linkTags;
	}

	public void setLinkTags(Set<LinkTag> linkTags) {
		this.linkTags = linkTags;
	}

	public Task getTask() {
		return task;
	}

	public void setTask(Task task) {
		this.task = task;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		TagType tagType = (TagType) o;

		if (name != null ? !name.equals(tagType.name) : tagType.name != null) return false;
		return task != null ? task.equals(tagType.task) : tagType.task == null;

	}

	@Override
	public int hashCode() {
		int result = name != null ? name.hashCode() : 0;
		result = 31 * result + (task != null ? task.hashCode() : 0);
		return result;
	}
}

