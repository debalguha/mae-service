/*
 * MAE - Multi-purpose Annotation Environment
 *
 * Copyright Keigh Rim (krim@brandeis.edu)
 * Department of Computer Science, Brandeis University
 * Original program by Amber Stubbs (astubbs@cs.brandeis.edu)
 *
 * MAE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, @see <a href="http://www.gnu.org/licenses">http://www.gnu.org/licenses</a>.
 *
 * For feedback, reporting bugs, use the project on Github
 * @see <a href="https://github.com/keighrim/mae-annotation">https://github.com/keighrim/mae-annotation</a>.
 */

package org.tworoads.mae.model;

import javax.persistence.*;

/**
 * Created by krim on 11/19/15.
 */

@Entity
@Table(name = "char_index", indexes = {@Index(name = "location_idx", columnList="location", unique = false)})
public class CharIndex implements ModelI {

	@Id
	@Column
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

    @Column(nullable = false)
    private int location;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "extenet_tag_id", referencedColumnName = "id")
    private ExtentTag tag;

    public CharIndex() {}

    public CharIndex(int location, ExtentTag tag) {
        this.setLocation(location);
        this.setTag(tag);
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getLocation() {
		return location;
	}

	public void setLocation(int location) {
		this.location = location;
	}

	public ExtentTag getTag() {
		return tag;
	}

	public void setTag(ExtentTag tag) {
		this.tag = tag;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		CharIndex charIndex = (CharIndex) o;

		if (location != charIndex.location) return false;
		return tag.equals(charIndex.tag);

	}

	@Override
	public int hashCode() {
		int result = location;
		result = 31 * result + (tag!=null?tag.hashCode():0);
		return result;
	}
}
