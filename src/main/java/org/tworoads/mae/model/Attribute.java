/*
 * MAE - Multi-purpose Annotation Environment
 *
 * Copyright Keigh Rim (krim@brandeis.edu)
 * Department of Computer Science, Brandeis University
 * Original program by Amber Stubbs (astubbs@cs.brandeis.edu)
 *
 * MAE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, @see <a href="http://www.gnu.org/licenses">http://www.gnu.org/licenses</a>.
 *
 * For feedback, reporting bugs, use the project on Github
 * @see <a href="https://github.com/keighrim/mae-annotation">https://github.com/keighrim/mae-annotation</a>.
 */

package org.tworoads.mae.model;

import java.util.List;

import javax.persistence.*;

/**
 * Created by krim on 11/19/15.
 */

@Entity
public class Attribute implements ModelI, ElementType {

	@Id
	@Column
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "attr_type_id", referencedColumnName = "id")
    private AttributeType attributeType;

	@Column(nullable = false)
    private String tid;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "extent_tag_id", referencedColumnName = "id")
    private ExtentTag extentTag;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "link_tag_id", referencedColumnName = "id")
    private LinkTag linkTag;

    @Column(nullable = false)
    private String value;

    public Attribute() {}

    public Attribute(AttributeType attributeType, String tid, ExtentTag extentTag, LinkTag linkTag, String value) {
        this.attributeType = attributeType;
        this.tid = tid;
        this.extentTag = extentTag;
        this.linkTag = linkTag;
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return String.format("%s - %s", this.attributeType.getName(), getValue());
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public AttributeType getAttributeType() {
		return attributeType;
	}

	public void setAttributeType(AttributeType attributeType) {
		this.attributeType = attributeType;
	}

	public String getTid() {
		return tid;
	}

	public void setTid(String tid) {
		this.tid = tid;
	}

	public ExtentTag getExtentTag() {
		return extentTag;
	}

	public void setExtentTag(ExtentTag extentTag) {
		this.extentTag = extentTag;
	}

	public LinkTag getLinkTag() {
		return linkTag;
	}

	public void setLinkTag(LinkTag linkTag) {
		this.linkTag = linkTag;
	}

    public String getName() {
        return this.attributeType.getName();
    }

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Attribute attribute = (Attribute) o;

		return tid != null ? tid.equals(attribute.tid) : attribute.tid == null;

	}

	@Override
	public int hashCode() {
		return tid != null ? tid.hashCode() : 0;
	}
}
