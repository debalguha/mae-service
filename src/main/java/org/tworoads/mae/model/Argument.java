/*
 * MAE - Multi-purpose Annotation Environment
 *
 * Copyright Keigh Rim (krim@brandeis.edu)
 * Department of Computer Science, Brandeis University
 * Original program by Amber Stubbs (astubbs@cs.brandeis.edu)
 *
 * MAE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, @see <a href="http://www.gnu.org/licenses">http://www.gnu.org/licenses</a>.
 *
 * For feedback, reporting bugs, use the project on Github
 * @see <a href="https://github.com/keighrim/mae-annotation">https://github.com/keighrim/mae-annotation</a>.
 */

package org.tworoads.mae.model;

import javax.persistence.*;

/**
 * Created by krim on 11/19/15.
 */

@Entity
public class Argument implements ModelI, ElementType {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "link_tag_id", referencedColumnName = "id")
    private LinkTag linkTag;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "extent_tag_id", referencedColumnName = "id")
    private ExtentTag extentTag;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "arg_type_id", referencedColumnName = "id")
    private ArgumentType argumentType;

    @Transient
    private String linkTid;

    @Transient
    private String extentTid;

    public Argument() {
    }

    public Argument(ArgumentType argumentType) {
        this.setArgumentType(argumentType);

    }

    public Argument(LinkTag linkTag, ArgumentType argumentType, ExtentTag extentTag) {
        this.linkTag = linkTag;
        this.extentTag = extentTag;
        this.argumentType = argumentType;
    }


    public LinkTag getLinkTag() {
        return linkTag;
    }

    public void setLinkTag(LinkTag linkTag) {
        this.linkTag = linkTag;
    }

    public ExtentTag getExtentTag() {
        return extentTag;
    }

    public void setExtentTag(ExtentTag extentTag) {
        this.extentTag = extentTag;
    }

    public ArgumentType getArgumentType() {
        return argumentType;
    }

    public void setArgumentType(ArgumentType argumentType) {
        this.argumentType = argumentType;
    }

    public boolean isComplete() {
        return getLinkTag() != null && getExtentTag() != null;
    }

    public String getName() {
        return this.getArgumentType().getName();
    }

    @Override
    public String toString() {
        return String.format("%s of %s",
                getArgumentType(), getLinkTag().getId());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLinkTid() {
        return linkTid;
    }

    public void setLinkTid(String linkTid) {
        this.linkTid = linkTid;
    }

    public String getExtentTid() {
        return extentTid;
    }

    public void setExtentTid(String extentTid) {
        this.extentTid = extentTid;
    }
}
