/*
 * MAE - Multi-purpose Annotation Environment
 *
 * Copyright Keigh Rim (krim@brandeis.edu)
 * Department of Computer Science, Brandeis University
 * Original program by Amber Stubbs (astubbs@cs.brandeis.edu)
 *
 * MAE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, @see <a href="http://www.gnu.org/licenses">http://www.gnu.org/licenses</a>.
 *
 * For feedback, reporting bugs, use the project on Github
 * @see <a href="https://github.com/keighrim/mae-annotation">https://github.com/keighrim/mae-annotation</a>.
 */

package org.tworoads.mae.model;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;


/**
 * Created by krim on 12/9/2015.
 */

@Entity
public class ArgumentType extends TagProperty implements ModelI {

    @OneToMany(fetch = FetchType.LAZY, mappedBy="argumentType")
    private Set<Argument> arguments;
    
    public ArgumentType() {}

    public Set<Argument> getArguments() {
        return arguments;
    }

    @Override
    public String toString() {
        return getName();

    }

    @Override
    public boolean equals(Object argumentType) {
        return argumentType instanceof ArgumentType && getName().equals(((ArgumentType) argumentType).getName());

    }
	public void setArguments(Set<Argument> arguments) {
		this.arguments = arguments;
	}

}
