/*
 * MAE - Multi-purpose Annotation Environment
 *
 * Copyright Keigh Rim (krim@brandeis.edu)
 * Department of Computer Science, Brandeis University
 * Original program by Amber Stubbs (astubbs@cs.brandeis.edu)
 *
 * MAE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, @see <a href="http://www.gnu.org/licenses">http://www.gnu.org/licenses</a>.
 *
 * For feedback, reporting bugs, use the project on Github
 * @see <a href="https://github.com/keighrim/mae-annotation">https://github.com/keighrim/mae-annotation</a>.
 */

package org.tworoads.mae.model;

import javax.persistence.*;

/**
 * Created by krim on 12/26/2015.
 */
@MappedSuperclass
public abstract class TagProperty implements ModelI {

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    protected Long id;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "tag_type_id", referencedColumnName = "id")
    protected TagType tagType;

    @Column(nullable = false)
    protected String name;

    @Column(nullable = false)
    protected boolean isRequired;

    @Column(nullable = false)
    protected boolean isIdRef;

    public TagProperty() {}

    public TagProperty(TagType tagType, String name, boolean isRequired, boolean isIdRef) {
        this.tagType = tagType;
        this.name = name;
        this.isRequired = isRequired;
        this.isIdRef = isIdRef;
    }

    public TagType getTagType() {
        return tagType;
    }

    public void setTagType(TagType tagType) {
        this.tagType = tagType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isIdRef() {
        return isIdRef;
    }

    public void setIdRef(boolean idRef) {
        isIdRef = idRef;
    }

    public boolean isRequired() {
        return isRequired;
    }

    public void setRequired(boolean required) {
        isRequired = required;
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TagProperty other = (TagProperty) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
    
    

}
