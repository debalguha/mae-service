/*
 * MAE - Multi-purpose Annotation Environment
 *
 * Copyright Keigh Rim (krim@brandeis.edu)
 * Department of Computer Science, Brandeis University
 * Original program by Amber Stubbs (astubbs@cs.brandeis.edu)
 *
 * MAE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, @see <a href="http://www.gnu.org/licenses">http://www.gnu.org/licenses</a>.
 *
 * For feedback, reporting bugs, use the project on Github
 * @see <a href="https://github.com/keighrim/mae-annotation">https://github.com/keighrim/mae-annotation</a>.
 */

package org.tworoads.mae.model;

import com.google.common.base.Strings;
import org.tworoads.mae.utils.MaeStrings;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import java.util.Arrays;
import java.util.Set;

/**
 * Created by krim on 11/19/15.
 */

@Entity
public class AttributeType extends TagProperty implements ModelI {

    @Column
    private String valueset;

    @Column(nullable = false)
    private String defaultValue;

    @OneToMany(fetch = FetchType.LAZY, mappedBy="attributeType")
    private Set<Attribute> attributes;


    public AttributeType() {}

    public AttributeType(TagType tagType, String name) {
        this.setTagType(tagType);
        this.setName(name);
        this.setRequired(false);
        this.setIdRef(false);
        this.defaultValue = "";
        this.valueset = null;
    }

    public String getValueset() {
        return valueset;
    }

    public boolean isFreeText() {
        return this.valueset == null;
    }

    public boolean isFiniteValueset() {
        return !this.isFreeText();
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public Set<Attribute> getAttributes() {
        return attributes;
    }

    public void setAttributes(Set<Attribute> attributes) {
        this.attributes = attributes;
    }

    public String toString() {
        return String.format("%s of %s - value: %s (%s), required: %s, idref: %s",
                getName(), getTagType().getName(), Strings.isNullOrEmpty(valueset) ? "": Arrays.asList(this.getValueset().split(MaeStrings.ATT_VALUESET_SEPARATOR)), getDefaultValue(), isRequired(), isIdRef());
    }

	public void setValueset(String valueset) {
		this.valueset = valueset;
	}


}
