package org.tworoads.mae;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.tworoads.mae.config.ApplictionConfiguration;

/**
 * Created by pc on 2/12/2017.
 */
@SpringBootApplication
//@Import(ApplictionConfiguration.class)
@Configuration
@PropertySource("${env}/application.properties")
@EnableAutoConfiguration
public class Main {
    public static void main(String args[]) throws Exception {
        System.setProperty("env", "local");
        try {
            SpringApplication.run(Main.class, args);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }
}
