package org.tworoads.mae.service;

import com.google.common.collect.Sets;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.tworoads.mae.AbstractBaseTest;
import org.tworoads.mae.model.ExtentTag;
import org.tworoads.mae.model.LinkTag;
import org.tworoads.mae.model.Task;

import java.io.File;
import java.util.Collection;
import java.util.Objects;

/**
 * Created by pc on 2/9/2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader=AnnotationConfigContextLoader.class)
public class MaeServiceTest extends AbstractBaseTest{
    @Configuration()
    @ComponentScan(basePackages = "org.tworoads.mae")
    static class ContextConfiguration {}
    @Autowired
    private MaeService service;
    private Task createdTask;
    @BeforeClass
    public static void setupClass(){
        System.setProperty("env", "local");
    }

    @After
    public void tearDown(){
        /*if(!Objects.isNull(createdTask))
            Assert.assertTrue(service.delete(createdTask));*/
    }

    @Test
    public void shouldBeAbleToCheckInitialization(){
        Assert.assertTrue(true);
    }

    @Test
    public void shouldBeAbleToCreateATaskFromJsonAndInsertIntoDB() throws Exception{
        createdTask = service.createTask(createTask());
        Assert.assertNotNull(createdTask);
    }

    private Task createTask() throws Exception{
        String taskJson = "{\"name\":\"NounVerbTask\",\"primaryText\":\"\\nMrs Miller wants the entire house repainted.\\n\",\"taskFileName\":\"C:\\\\Users\\\\dguha\\\\git\\\\mae-annotation\\\\samples\\\\sampleTask.dtd\",\"annotationFileName\":\"C:\\\\Users\\\\dguha\\\\git\\\\mae-annotation\\\\samples\\\\miller.xml\"}";
        String extentTagJson = "[{\"tid\":\"N0\",\"tagType\":{\"name\":\"NOUN\",\"prefix\":\"N\",\"link\":false,\"nonConsuming\":true},\"filename\":\"C:\\\\Users\\\\pc\\\\git\\\\mae-annotation\\\\samples\\\\miller.xml\",\"attributes\":[{\"attributeType\":{\"tagType\":{\"name\":\"NOUN\",\"prefix\":\"N\",\"link\":false,\"nonConsuming\":true},\"name\":\"type\",\"valueset\":\"person:::place:::thing:::other\",\"defaultValue\":\"other\",\"required\":false,\"idRef\":false},\"tid\":\"N0\",\"value\":\"other\"},{\"attributeType\":{\"tagType\":{\"name\":\"NOUN\",\"prefix\":\"N\",\"link\":false,\"nonConsuming\":true},\"name\":\"comment\",\"valueset\":null,\"defaultValue\":\"default value\",\"required\":false,\"idRef\":false},\"tid\":\"N0\",\"value\":\"default value\"}],\"text\":\"Mrs Miller\",\"spans\":[{\"location\":1},{\"location\":2},{\"location\":3},{\"location\":4},{\"location\":5},{\"location\":6},{\"location\":7},{\"location\":8},{\"location\":9},{\"location\":10}]},{\"tid\":\"N1\",\"tagType\":{\"name\":\"NOUN\",\"prefix\":\"N\",\"link\":false,\"nonConsuming\":true},\"filename\":\"C:\\\\Users\\\\pc\\\\git\\\\mae-annotation\\\\samples\\\\miller.xml\",\"attributes\":[{\"attributeType\":{\"tagType\":{\"name\":\"NOUN\",\"prefix\":\"N\",\"link\":false,\"nonConsuming\":true},\"name\":\"type\",\"valueset\":\"person:::place:::thing:::other\",\"defaultValue\":\"other\",\"required\":false,\"idRef\":false},\"tid\":\"N1\",\"value\":\"other\"},{\"attributeType\":{\"tagType\":{\"name\":\"NOUN\",\"prefix\":\"N\",\"link\":false,\"nonConsuming\":true},\"name\":\"comment\",\"valueset\":null,\"defaultValue\":\"default value\",\"required\":false,\"idRef\":false},\"tid\":\"N1\",\"value\":\"default value\"}],\"text\":\"house\",\"spans\":[{\"location\":29},{\"location\":30},{\"location\":31},{\"location\":32},{\"location\":33}]},{\"tid\":\"N2\",\"tagType\":{\"name\":\"NOUN\",\"prefix\":\"N\",\"link\":false,\"nonConsuming\":true},\"filename\":\"C:\\\\Users\\\\pc\\\\git\\\\mae-annotation\\\\samples\\\\miller.xml\",\"attributes\":[{\"attributeType\":{\"tagType\":{\"name\":\"NOUN\",\"prefix\":\"N\",\"link\":false,\"nonConsuming\":true},\"name\":\"type\",\"valueset\":\"person:::place:::thing:::other\",\"defaultValue\":\"other\",\"required\":false,\"idRef\":false},\"tid\":\"N2\",\"value\":\"other\"},{\"attributeType\":{\"tagType\":{\"name\":\"NOUN\",\"prefix\":\"N\",\"link\":false,\"nonConsuming\":true},\"name\":\"comment\",\"valueset\":null,\"defaultValue\":\"default value\",\"required\":false,\"idRef\":false},\"tid\":\"N2\",\"value\":\"default value\"}],\"text\":\"\",\"spans\":[]},{\"tid\":\"V0\",\"tagType\":{\"name\":\"VERB\",\"prefix\":\"V\",\"link\":false,\"nonConsuming\":false},\"filename\":\"C:\\\\Users\\\\pc\\\\git\\\\mae-annotation\\\\samples\\\\miller.xml\",\"attributes\":[{\"attributeType\":{\"tagType\":{\"name\":\"VERB\",\"prefix\":\"V\",\"link\":false,\"nonConsuming\":false},\"name\":\"aspect\",\"valueset\":\"simple:::progressive:::perfect:::perfect progressive\",\"defaultValue\":\"perfect progressive\",\"required\":false,\"idRef\":false},\"tid\":\"V0\",\"value\":\"perfect progressive\"}],\"text\":\"wants\",\"spans\":[{\"location\":12},{\"location\":13},{\"location\":14},{\"location\":15},{\"location\":16}]},{\"tid\":\"A0\",\"tagType\":{\"name\":\"ADJ_ADV\",\"prefix\":\"A\",\"link\":false,\"nonConsuming\":false},\"filename\":\"C:\\\\Users\\\\pc\\\\git\\\\mae-annotation\\\\samples\\\\miller.xml\",\"attributes\":[],\"text\":\"repainted\",\"spans\":[{\"location\":35},{\"location\":36},{\"location\":37},{\"location\":38},{\"location\":39},{\"location\":40},{\"location\":41},{\"location\":42},{\"location\":43}]},{\"tid\":\"A1\",\"tagType\":{\"name\":\"ADJ_ADV\",\"prefix\":\"A\",\"link\":false,\"nonConsuming\":false},\"filename\":\"C:\\\\Users\\\\pc\\\\git\\\\mae-annotation\\\\samples\\\\miller.xml\",\"attributes\":[],\"text\":\"entire\",\"spans\":[{\"location\":22},{\"location\":23},{\"location\":24},{\"location\":25},{\"location\":26},{\"location\":27}]}]";
        String linkTagJson = "[{\"tid\":\"AC1\",\"tagType\":{\"name\":\"ACTION\",\"prefix\":\"AC\",\"link\":true,\"nonConsuming\":false},\"filename\":\"C:\\\\Users\\\\dguha\\\\git\\\\mae-annotation\\\\samples\\\\miller.xml\",\"attributes\":[{\"attributeType\":{\"tagType\":{\"name\":\"ACTION\",\"prefix\":\"AC\",\"link\":true,\"nonConsuming\":false},\"name\":\"relationship\",\"valueset\":\"performs:::performed_by\",\"defaultValue\":\"\",\"required\":false,\"idRef\":false},\"tid\":\"AC1\",\"value\":\"performs\"}],\"arguments\":[{\"argumentType\":{\"tagType\":{\"name\":\"ACTION\",\"prefix\":\"AC\",\"link\":true,\"nonConsuming\":false},\"name\":\"from\",\"required\":false,\"idRef\":true},\"extentTid\":\"N2\",\"linkTid\":\"AC1\"},{\"argumentType\":{\"tagType\":{\"name\":\"ACTION\",\"prefix\":\"AC\",\"link\":true,\"nonConsuming\":false},\"name\":\"to\",\"required\":false,\"idRef\":true},\"extentTid\":\"A0\",\"linkTid\":\"AC1\"}]},{\"tid\":\"D1\",\"tagType\":{\"name\":\"DESCRIPTION\",\"prefix\":\"D\",\"link\":true,\"nonConsuming\":false},\"filename\":\"C:\\\\Users\\\\dguha\\\\git\\\\mae-annotation\\\\samples\\\\miller.xml\",\"attributes\":[{\"attributeType\":{\"tagType\":{\"name\":\"DESCRIPTION\",\"prefix\":\"D\",\"link\":true,\"nonConsuming\":false},\"name\":\"relationship\",\"valueset\":\"describes:::described_by\",\"defaultValue\":\"\",\"required\":false,\"idRef\":false},\"tid\":\"D1\",\"value\":\"described_by\"}],\"arguments\":[{\"argumentType\":{\"tagType\":{\"name\":\"DESCRIPTION\",\"prefix\":\"D\",\"link\":true,\"nonConsuming\":false},\"name\":\"arg0\",\"required\":true,\"idRef\":true},\"extentTid\":\"N1\",\"linkTid\":\"D1\"},{\"argumentType\":{\"tagType\":{\"name\":\"DESCRIPTION\",\"prefix\":\"D\",\"link\":true,\"nonConsuming\":false},\"name\":\"arg1\",\"required\":false,\"idRef\":true},\"extentTid\":\"A1\",\"linkTid\":\"D1\"},{\"argumentType\":{\"tagType\":{\"name\":\"DESCRIPTION\",\"prefix\":\"D\",\"link\":true,\"nonConsuming\":false},\"name\":\"arg2\",\"required\":false,\"idRef\":true},\"extentTid\":\"A0\",\"linkTid\":\"D1\"}]},{\"tid\":\"AR0\",\"tagType\":{\"name\":\"ARGUMENTS\",\"prefix\":\"AR\",\"link\":true,\"nonConsuming\":false},\"filename\":\"C:\\\\Users\\\\dguha\\\\git\\\\mae-annotation\\\\samples\\\\miller.xml\",\"attributes\":[{\"attributeType\":{\"tagType\":{\"name\":\"ARGUMENTS\",\"prefix\":\"AR\",\"link\":true,\"nonConsuming\":false},\"name\":\"has_gap\",\"valueset\":\"yes:::no\",\"defaultValue\":\"\",\"required\":false,\"idRef\":false},\"tid\":\"AR0\",\"value\":\"no\"}],\"arguments\":[{\"argumentType\":{\"tagType\":{\"name\":\"ARGUMENTS\",\"prefix\":\"AR\",\"link\":true,\"nonConsuming\":false},\"name\":\"agent\",\"required\":true,\"idRef\":true},\"extentTid\":\"N0\",\"linkTid\":\"AR0\"},{\"argumentType\":{\"tagType\":{\"name\":\"ARGUMENTS\",\"prefix\":\"AR\",\"link\":true,\"nonConsuming\":false},\"name\":\"patient\",\"required\":true,\"idRef\":true},\"extentTid\":\"N1\",\"linkTid\":\"AR0\"},{\"argumentType\":{\"tagType\":{\"name\":\"ARGUMENTS\",\"prefix\":\"AR\",\"link\":true,\"nonConsuming\":false},\"name\":\"theme\",\"required\":true,\"idRef\":true},\"extentTid\":\"A0\",\"linkTid\":\"AR0\"},{\"argumentType\":{\"tagType\":{\"name\":\"ARGUMENTS\",\"prefix\":\"AR\",\"link\":true,\"nonConsuming\":false},\"name\":\"predicate\",\"required\":true,\"idRef\":true},\"extentTid\":\"V0\",\"linkTid\":\"AR0\"}]}]";
        Task task = mapper.readValue(taskJson, Task.class);
        task.setAnnotationFileContent(FileUtils.readFileToByteArray(new File(task.getAnnotationFileName())));
        task.setTaskFileContent(FileUtils.readFileToByteArray(new File(task.getTaskFileName())));
        Collection<ExtentTag> extentTags = deserializeJsonToClass(extentTagJson, ExtentTag.class);
        Collection<LinkTag> linkTags = deserializeJsonToClass(linkTagJson, LinkTag.class);
        linkTags.forEach(tag -> tag.setTask(task));
        extentTags.forEach(tag -> tag.setTask(task));
        task.setExtentTags(Sets.newHashSet(extentTags));
        task.setLinkTags(Sets.newHashSet(linkTags));
        return task;
    }
}
