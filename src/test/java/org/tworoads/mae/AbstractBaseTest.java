package org.tworoads.mae;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

/**
 * Created by pc on 2/10/2017.
 */
public class AbstractBaseTest {
    protected ObjectMapper mapper;

    @Before
    public void setup(){
        mapper = new ObjectMapper();
    }
    public <T> Collection<T> deserializeJsonToClass(String json, Class<T> objectClass) throws IOException {
        JavaType type = mapper.getTypeFactory().constructCollectionType(List.class, objectClass);
        return mapper.readValue(json, type);
    }
}
